# Scramble Words 

### _assigment date: 31.10.2022 9:00_

### _due date: 7.11.2022 8:30_

## Summary

This is a word scramble game. The game is made with JavaScript, HTML and CSS.

- `index.html` - the main HTML file
- `style.css` - the main CSS file
- `script.js` - the main JS file
- `words.js` - the JS file containing the words

## Steps to reproduce

Open the `index.html` file in your browser and follow the game instructions.

### SCRAMBLE WORDS REQUIREMENTS:

- at least 3 different game topics
- minimum 3 levels of difficulty (words of 6, 7, 8 letters, etc.).
  - no less than 6 letters in a word
- find at least 10 words on each level so you can move on to the next level.
- maximum 5 attempts to guess the word
  - if it was guessed in 5 attempts, move on to the next word
  - if it was not guessed, it is reset to the level you are at and it is taken from the first word
- to get a new attempt per word, play rock, paper, scissors against a bot. If you win you have one more try, if you don't win you have to wait 30 seconds before you can play again.
- responsive
- UX friendly
- at least one optional

### OPTIONALS:

- as a level of difficulty, the selection of the language in which to display the words can be entered
- a "practice" module => where you have some easy words to get used to the game system (here they can be words of 4 or 5 letters)
- a module for children => to arrange the letters for a word found in an image
- a module "arranging scrambled words" => the module where you can arrange words to get phrases.
- a module with hints (easy type) => where besides the "scrambled" letters you also have a hint to what the word represents (a city, an animal, a profession.)

---

## Reviewers

- [Creanga Alexandru](creanga.alexandru@ivfa.ro) -
- [ ] @

---

## Authors

- [Gorobtov Vlad Andrei](gorobtov.vlad@ivfa.ro)
- [Vatafu Radu-Ionel](vatafu.radu@ivfa.ro)
- [Constantinescu Dragos](constantinescu.dragos@ivfa.ro)
- [Alina-Ioana Popescu](popescu.alina@ivfa.ro)
- [Nicoleta Mazarel](mazarel.nicoleta@ivfa.ro)

## Bugs


