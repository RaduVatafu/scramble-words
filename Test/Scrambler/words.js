let wordsPractice = [
    {
        word: "table",
        hint: "You put things on it"
    },
    {
        word: "wall",
        hint: "Buildind structure"
    },
    {
        word: "tree",
        hint: "Natural thing made from wood"
    },
    {
        word: "plane",
        hint: "Transport passengers in the sky"
    }
];
let wordsPracticeRo = [
    {
        word: "masa",
        hint: "Obiect pe care pui lucruri"
    },
    {
        word: "perete",
        hint: "Structura a unei cladiri"
    },
    {
        word: "copac",
        hint: "Lucru natural facut din lemn"
    },
    {
        word: "avion",
        hint: "Transporta pasageri in zbor"
    }
];

let citiesEasy = [
    {
        word: "london",
        hint: "England"
    },
    {
        word: "istanbul",
        hint: "Constantinople"
    },
    {
        word: "barcelona",
        hint: "Catalonia"
    },
    {
        word: "madrid",
        hint: "central part of the Iberian Peninsula"
    },
    {
        word: "moscow",
        hint: "Largest city of Russia"
    },
    {
        word: "berlin",
        hint: "Germany"
    },
    {
        word: "munich",
        hint: "capital of Bavaria"
    },
    {
        word: "budapest",
        hint: "Hungary"
    },
    {
        word: "bangkok",
        hint: "Thailand"
    },
    {
        word: "singapore",
        hint: "city-state in maritime Southeast Asia"
    },
    {
        word: "bucharest",
        hint: "Romania"
    },
    {
        word: "zagreb",
        hint: "Croatia"
    },
    {
        word: "nicosia",
        hint: "Cyprus"
    },
    {
        word: "vienna",
        hint: "Austria"
    },
    {
        word: "ottawa",
        hint: "Canada"
    },
    {
        word: "helsinki",
        hint: "Finland"
    },
    {
        word: "brasilia",
        hint: "Brazil"
    }
];

let citiesMedium = [
    {
        word: "detroit",
        hint: "largest city in the U.S. state of Michigan"
    },
    {
        word: "chicago",
        hint: "the third-most populous in the United States"
    },
    {
        word: "talinn",
        hint: "Estonia"
    },
    {
        word: "seville",
        hint: "largest city of Andalusia"
    },
    {
        word: "yerevan",
        hint: "Armenia"
    },
    {
        word: "canberra",
        hint: "Australia"
    },
    {
        word: "bogota",
        hint: "Colombia"
    },
    {
        word: "santiago",
        hint: "Chile"
    },
    {
        word: "jakarta",
        hint: "Indonesia"
    },
    {
        word: "jerusalem",
        hint: "one of the oldest cities in the world"
    },
    {
        word: "riyadh",
        hint: "Saudi Arabia"
    },
    {
        word: "vatican",
        hint: "Sistine Chapel"
    },
    {
        word: "caracas",
        hint: "Venezuela"
    },
    {
        word: "prague",
        hint: "historical capital of Bohemia"
    }
]

let citiesHard = [
    {
        word: "antwerp",
        hint: "the second-largest metropolitan region in Belgium"
    },
    {
        word: "reykjavik",
        hint: "Iceland"
    },
    {
        word: "ljubljana",
        hint: "Slovenia"
    },
    {
        word: "bordeaux",
        hint: "known in the world for its wines and vineyards"
    },
    {
        word: "salzburg",
        hint: "Austria"
    },
    {
        word: "oaxaca",
        hint: "southwestern Mexico"
    },
    {
        word: "thimphu",
        hint: "Bhutan"
    },
    {
        word: "sarajevo",
        hint: "Bosnia and Herzegovina"
    },
    {
        word: "copenhagen",
        hint: "Denmark"
    },
    {
        word: "tegucigalpa",
        hint: "largest city of Honduras"
    },
    {
        word: "vilnius",
        hint: "Lithuania"
    },
    {
        word: "podgorica",
        hint: "Montenegro"
    },
    {
        word: "kathmandu",
        hint: "Nepal"
    },
    {
        word: "wellington",
        hint: "New Zealand"
    },
    {
        word: "edinburgh",
        hint: "Scotland"
    },
    {
        word: "bratislava",
        hint: "Slovakia"
    }
]

let citiesEasyRo = [
    {
        word: "londra",
        hint: "Anglia"
    },
    {
        word: "istanbul",
        hint: "Constantinopol"
    },
    {
        word: "barcelona",
        hint: "Catalonia"
    },
    {
        word: "madrid",
        hint: "in partea centrala a peninsulei iberice"
    },
    {
        word: "moscova",
        hint: "unul dintre cele mai populate orase din Europa"
    },
    {
        word: "berlin",
        hint: "Germania"
    },
    {
        word: "munchen",
        hint: "capitala Bavariei"
    },
    {
        word: "budapesta",
        hint: "Ungaria"
    },
    {
        word: "bangkok",
        hint: "Thailanda"
    },
    {
        word: "singapore",
        hint: "oraș-stat din Asia de Sud-Est"
    },
    {
        word: "bucuresti",
        hint: "Romania"
    },
    {
        word: "zagreb",
        hint: "Croatia"
    },
    {
        word: "nicosia",
        hint: "Cipru"
    },
    {
        word: "ottawa",
        hint: "Canada"
    },
    {
        word: "helsinki",
        hint: "Finlanda"
    },
    {
        word: "brasilia",
        hint: "Brazilia"
    }
];

let citiesMediumRo = [
    {
        word: "detroit",
        hint: "cel mai mare oras din statul american Michigan"
    },
    {
        word: "chicago",
        hint: "al treilea cel mai populat din Statele Unite"
    },
    {
        word: "sevilia",
        hint: "cel mai mare oraș din Andaluzia"
    },
    {
        word: "erevan",
        hint: "Armenia"
    },
    {
        word: "canberra",
        hint: "Australia"
    },
    {
        word: "bogota",
        hint: "Columbia"
    },
    {
        word: "santiago",
        hint: "Chile"
    },
    {
        word: "jakarta",
        hint: "Indonezia"
    },
    {
        word: "ierulasim",
        hint: "unul din cele mai vechi orase din lume"
    },
    {
        word: "vatican",
        hint: "capela sixtina"
    },
    {
        word: "caracas",
        hint: "Venezuela"
    }
]

let citiesHardRo = [
    {
        word: "anvers",
        hint: "a doua regiune metropolitana ca marime din Belgia"
    },
    {
        word: "reykjavik",
        hint: "Islanda"
    },
    {
        word: "ljubljana",
        hint: "Slovenia"
    },
    {
        word: "bordeaux",
        hint: "cunoscut in lume pentru vinurile si potgoriile sale"
    },
    {
        word: "salzburg",
        hint: "Austria"
    },
    {
        word: "oaxaca",
        hint: "sud-vestul Mexicului"
    },
    {
        word: "thimphu",
        hint: "Bhutan"
    },
    {
        word: "sarajevo",
        hint: "Bosnia si Hertegovina"
    },
    {
        word: "copenhaga",
        hint: "Danemarca"
    },
    {
        word: "tegucigalpa",
        hint: "cel mai mare oraș din Honduras"
    },
    {
        word: "vilnius",
        hint: "Lituania"
    },
    {
        word: "podgorica",
        hint: "Muntenegru"
    },
    {
        word: "kathmandu",
        hint: "Nepal"
    },
    {
        word: "wellington",
        hint: "Noua Zeelanda"
    },
    {
        word: "edinburgh",
        hint: "Scotia"
    },
    {
        word: "bratislava",
        hint: "Slovacia"
    }
]

let logosEasy = [
    {
        word: "microsoft",
        hint: "microcomputer and software"
    },
    {
        word: "mcdonalds",
        hint: "fast food chain"
    },
    {
        word: "toyota",
        hint: "Japanese automotive manufacturer"
    },
    {
        word: "samsung",
        hint: "South Korean electronics corporation"
    },
    {
        word: "firefox",
        hint: "web-browser"
    },
    {
        word: "google",
        hint: "company focusing on search engine technology"
    },
    {
        word: "instagram",
        hint: "instant camera and telegram"
    },
    {
        word: "whatsapp",
        hint: "instant messaging application"
    },
    {
        word: "telegram",
        hint: "popular cross-platform messaging app"
    },
    {
        word: "amazon",
        hint: "technology company focusing on e-commerce"
    },
    {
        word: "twitter",
        hint: "free social networking site"
    },
    {
        word: "oracle",
        hint: "sells database software and technology"
    },
    {
        word: "armani",
        hint: "italian luxury fashion house"
    },
    {
        word: "balenciaga",
        hint: "spanish luxury fashion house"
    },
    {
        word: "ferrari",
        hint: "italian luxury sport cars"
    }
]

let logosMedium = [
    {
        word: "ubuntu",
        hint: "a Linux distribution"
    },
    {
        word: "carrefour",
        hint: "French multinational retail"
    },
    {
        word: "volkswagen",
        hint: "German motor vehicle manufacturer "
    },
    {
        word: "colgate",
        hint: "world leader in the production of toothpaste"
    },
    {
        word: "hyundai",
        hint: "automobile manufacturer"
    },
    {
        word: "panasonic",
        hint: "Japanese brand of electronic products"
    },
    {
        word: "bentley",
        hint: "British designer, manufacturer and marketer of luxury cars"
    },
    {
        word: "mastercard",
        hint: "the second-largest payment-processing corporation worldwide"
    },
    {
        word: "pinterest",
        hint: "website for sharing and categorizing images found online"
    },
    {
        word: "gillette",
        hint: "type of razor and other products for shaving"
    },
    {
        word: "motorola",
        hint: "company that makes telephones and computer equipment"
    },
    {
        word: "nestle",
        hint: "large international company that makes Nescafe coffee"
    },
    {
        word: "loreal",
        hint: "French personal care company"
    },
    {
        word: "cartier",
        hint: "French jewellery and watch manufacturer"
    },
    {
        word: "corona",
        hint: "type of Mexican beer"
    }
]

let logosHard = [
    {
        word: "marlboro",
        hint: "American brand of cigarettes"
    },
    {
        word: "starbucks",
        hint: "American multinational chain of coffeehouses"
    },
    {
        word: "porsche",
        hint: "German automobile manufacturer"
    },
    {
        word: "chevrolet",
        hint: "type of US car, made by General Motors"
    },
    {
        word: "danone",
        hint: "French multinational food-products corporation"
    },
    {
        word: "paypal",
        hint: "American company operating an online payments system"
    },
    {
        word: "heineken",
        hint: "company from the Netherlands in the beer industry"
    },
    {
        word: "allianz",
        hint: "one of the world's largest insurers"
    },
    {
        word: "huawei",
        hint: "Chinese multinational technology corporation"
    },
    {
        word: "michelin",
        hint: "French tires manufacturer"
    },
    {
        word: "wikipedia",
        hint: "multilingual free online encyclopedia"
    },
    {
        word: "mitsubishi",
        hint: "Japanese automaker"
    },
    {
        word: "blackberry",
        hint: "former brand of smartphones"
    }
]

let logosEasyRo = [
    {
        word: "microsoft",
        hint: "microcomputer si software"
    },
    {
        word: "mcdonalds",
        hint: "lant fast food"
    },
    {
        word: "toyota",
        hint: "Producator japonez de automobile"
    },
    {
        word: "samsung",
        hint: "corporatie din Coreea de Sud producatoare de electronice"
    },
    {
        word: "firefox",
        hint: "web-browser"
    },
    {
        word: "google",
        hint: "motor de cautare pe internet"
    },
    {
        word: "instagram",
        hint: "instant camera si telegram"
    },
    {
        word: "whatsapp",
        hint: "instant messaging application"
    },
    {
        word: "telegram",
        hint: "serviciu de mesagerie"
    },
    {
        word: "amazon",
        hint: "companie care se concentreaza pe e-commerce"
    },
    {
        word: "twitter",
        hint: "retea sociala"
    },
    {
        word: "oracle",
        hint: "sistem de management al bazelor de date"
    },
    {
        word: "armani",
        hint: "casa de moda de lux italiana"
    },
    {
        word: "balenciaga",
        hint: "casa de moda de lux spaniola"
    },
    {
        word: "ferrari",
        hint: "masini sport italiene de lux"
    }
]

let logosMediumRo = [
    {
        word: "ubuntu",
        hint: "Linux"
    },
    {
        word: "carrefour",
        hint: "retailer francez"
    },
    {
        word: "volkswagen",
        hint: "producator german de autovehicule"
    },
    {
        word: "colgate",
        hint: "lider mondial în productia de pasta de dinti"
    },
    {
        word: "hyundai",
        hint: "producator de automobile"
    },
    {
        word: "panasonic",
        hint: "marca japoneza de produse electronice"
    },
    {
        word: "bentley",
        hint: "Designer, producator si distribuitor britanic de masini de lux"
    },
    {
        word: "mastercard",
        hint: "a doua cea mai mare corporatie de procesare a platilor din lume"
    },
    {
        word: "pinterest",
        hint: "site pentru partajarea si clasificarea imaginilor gasite online"
    },
    {
        word: "gillette",
        hint: "tip de aparat de ras si alte produse pentru barbierit"
    },
    {
        word: "motorola",
        hint: "companie care produce telefoane si echipamente informatice"
    },
    {
        word: "nestle",
        hint: "producator al cafelei Nescafe"
    },
    {
        word: "loreal",
        hint: "companie franceza de ingrijire personala"
    },
    {
        word: "cartier",
        hint: "Producator francez de bijuterii si ceasuri"
    },
    {
        word: "corona",
        hint: "tip de bere mexicana"
    }
]

let logosHardRo = [
    {
        word: "marlboro",
        hint: "Marca americana de tigari"
    },
    {
        word: "starbucks",
        hint: "Lant multinational american de cafenele"
    },
    {
        word: "porsche",
        hint: "producator german de automobile"
    },
    {
        word: "chevrolet",
        hint: "tip de masina din SUA, fabricata de General Motors"
    },
    {
        word: "danone",
        hint: "Companie multinationala franceza de produse alimentare"
    },
    {
        word: "paypal",
        hint: "sistem de plati online"
    },
    {
        word: "heineken",
        hint: "companie din Olanda din industria berii"
    },
    {
        word: "allianz",
        hint: "unul dintre cei mai mari asiguratori din lume"
    },
    {
        word: "huawei",
        hint: "companie chineza producatoare de echipamente de telecomunicatii"
    },
    {
        word: "michelin",
        hint: "fabricant de anvelope din Franta"
    },
    {
        word: "wikipedia",
        hint: "enciclopedie online gratuita multilingva"
    },
    {
        word: "mitsubishi",
        hint: "Producator auto japonez"
    },
    {
        word: "blackberry",
        hint: "fosta marca de smartphone-uri"
    }
]

let foodEasy = [
    {
        word: "apricot",
        hint: "a juicy, soft fruit resembling a small peach"
    },
    {
        word: "banana",
        hint: "long curved fruit which grows in clusters"
    },
    {
        word: "blackberry",
        hint: "purple-black soft fruit"
    },
    {
        word: "broccoli",
        hint: "green vegetable in the cabbage family"
    },
    {
        word: "chicken",
        hint: "domestic fowl kept for its eggs or meat"
    },
    {
        word: "chocolate",
        hint: "dessert"
    },
    {
        word: "dressing",
        hint: "a sauce for salads"
    },
    {
        word: "doughnut",
        hint: "a small fried cake of sweetened dough"
    },
    {
        word: "eggplant",
        hint: "an oval, purple vegetable"
    },
    {
        word: "grapefruit",
        hint: "somewhat bitter fruit"
    },
    {
        word: "gingerbread",
        hint: "broad category of baked goods typically flavored with ginger"
    },
    {
        word: "hamburger",
        hint: "food consisting of meat placed inside a sliced bun"
    },
    {
        word: "kitchen",
        hint: "room used for cooking"
    },
    {
        word: "lemonade",
        hint: "a sweetened lemon-flavored beverage"
    },
    {
        word: "lasagna",
        hint: "a type of pasta made of very wide, flat sheets"
    },
]

let foodMedium = [
    {
        word: "zucchini",
        hint: "a variety of summer squash that is shaped like a cucumber"
    },
    {
        word: "asparagus",
        hint: "a tall plant cultivated for its edible shoots"
    },
    {
        word: "turmeric",
        hint: "a bright yellow aromatic powder"
    },
    {
        word: "bechamel",
        hint: "a white sauce, made with butter, flour, and milk"
    },
    {
        word: "spareribs",
        hint: "the ribs of a pig, with most of the meat removed"
    },
    {
        word: "scallop",
        hint: "a sea creature that lives inside two joined flat shells"
    },
    {
        word: "rosemary",
        hint: "a bush whose leaves are used to add flavour in cooking"
    },
    {
        word: "pitcher",
        hint: "a container for holding liquids"
    },
    {
        word: "parsnip",
        hint: "plant with long cream-coloured root"
    },
    {
        word: "oatmeal",
        hint: "a type of flour made from oats"
    },
    {
        word: "spinach",
        hint: "vegetable with wide, dark green leaves that are eaten cooked or uncooked"
    },
    {
        word: "pumpkin",
        hint: "a large rounded orange-yellow fruit with a thick rind"
    },
    {
        word: "jalapeno",
        hint: "a very hot green chilli pepper"
    },
    {
        word: "pistachio",
        hint: "a nut with a hard shell containing a green seed that can be eaten"
    },
    {
        word: "cheddar",
        hint: "natural cheese that is relatively hard, off-white and sometimes sharp-tasting"
    }
]

let foodHard = [
    {
        word: "venison",
        hint: "meat that comes from a deer"
    },
    {
        word: "tarragon",
        hint: "a plant with narrow aromatic leaves"
    },
    {
        word: "tamale",
        hint: "a Mexican dish of seasoned meat and maize flour"
    },
    {
        word: "quiche",
        hint: "French tart"
    },
    {
        word: "molasses",
        hint: "thick, dark brown juice obtained from raw sugar"
    },
    {
        word: "drumstick",
        hint: "the lower part of the leg of a chicken"
    },
    {
        word: "horseradish",
        hint: "a plant with white root that has a strong sharp taste"
    },
    {
        word: "celery",
        hint: "a vegetable with long, thin, whitish or pale green stems"
    },
    {
        word: "cauliflower",
        hint: "a large, round, white vegetable"
    },
    {
        word: "turnip",
        hint: "vegetable grown for its juicy, bulbous root"
    },
    {
        word: "minestrone",
        hint: "italian soup"
    }
]

let foodEasyRo = [
    {
        word: "banana",
        hint: "fruct lung curbat care creste în ciorchini"
    },
    {
        word: "brocoli",
        hint: "leguma verde din familia verzei"
    },
    {
        word: "ciocolata",
        hint: "desert"
    },
    {
        word: "gogoasa",
        hint: "gustare facuta din aluat indulcit"
    },
    {
        word: "vanata",
        hint: "legumă ovala, violet"
    },
    {
        word: "grepfrut",
        hint: "fruct oarecum amar"
    },
    {
        word: "hamburger",
        hint: "mancare formata din carne asezata in interiorul unei chifle feliate"
    },
    {
        word: "bucatarie",
        hint: "camera in care se gateste"
    },
    {
        word: "limonada",
        hint: "bautura indulcita cu aroma de lamaie"
    },
    {
        word: "lasagna",
        hint: "tip de paste facute din foi largi, plate"
    },
    {
        word: "telina",
        hint: "o leguma cu tulpini lungi, subtiri, albicioase sau verde pal"
    },
    {
        word: "conopida",
        hint: "leguma mare, rotunda, alba"
    },
    {
        word: "ridiche",
        hint: "leguma cultivata pentru radacina ei suculenta si bulboasa"
    },
    {
        word: "dovlecel",
        hint: "un tip de leguma de vara de forma unui castravete"
    }
]

let foodMediumRo = [
    {
        word: "dressing",
        hint: "sos pentru salata"
    },
    {
        word: "sparanghel",
        hint: "planta inalta cultivata pentru lastarii ei comestibili"
    },
    {
        word: "turmeric",
        hint: "praf aromatic galben"
    },
    {
        word: "bechamel",
        hint: "sos alb facut cu unt, faina si lapte"
    },
    {
        word: "rozmarin",
        hint: "planta ale carei frunze sunt folosite pentru a adauga aroma la gatit"
    },
    {
        word: "ulcior",
        hint: "recipient pentru pastrarea lichidelor"
    },
    {
        word: "pastarnac",
        hint: "planta cu radacina lunga de culoare crem"
    },
    {
        word: "spanac",
        hint: "leguma cu frunze mari de culoare verde inchis"
    },
    {
        word: "dovleac",
        hint: "fruct mare, rotund, galben-portocaliu, cu coaja groasa"
    },
    {
        word: "cheddar",
        hint: "probabil cea mai consumata branza englezeasca din lume"
    },
    {
        word: "oliviera",
        hint: "utilizata pentru a oferi ulei si otet consumatorilor unui restaurant"
    }
]

let foodHardRo = [
    {
        word: "tarhon",
        hint: "o planta cu frunze aromatice inguste"
    },
    {
        word: "tamale",
        hint: "preparat mexican din carne condimentata si faina de porumb"
    },
    {
        word: "quiche",
        hint: "tarta frantuzeasca"
    },
    {
        word: "melasa",
        hint: "suc gros, maro inchis, obtinut din zahar brut"
    },
    {
        word: "antricot",
        hint: "carne de vaca, de oaie sau de porc provenita din regiunea intercostala"
    },
    {
        word: "aspartam",
        hint: "substanta folosita ca îndulcitor"
    },
    {
        word: "gorgonzola",
        hint: "branza de origine italiana"
    },
    {
        word: "maghiran",
        hint: "planta ierboasa aromatica"
    },
    {
        word: "minestrone",
        hint: "supa specific italiana"
    },
    {
        word: "picromigdala",
        hint: "prajitura uscata preparata din albus de ou, zahar si migdale"
    },
    {
        word: "stufat",
        hint: "mancare din carne de miel, ceapa si usturoi verde"
    } 
]

let kidsWords = [
    {
        word: "monkey",
        hint:  "./images/monkey.png"
    },
    {
        word: "icecream",
        hint: "./images/icecream.png"
    }, 
    {
        word: "horse",
        hint: "./images/horse.png"
    }, 
    {
        word: "bicycle",
        hint: "./images/bicicle.png"
    }, 
    {
        word: "rainbow",
        hint: "./images/rainbow.png"
    }, 
    {
        word: "book",
        hint: "./images/book.png"
    }, 
    {
        word: "banana",
        hint: "./images/banana.png"
    }, 
    {
        word: "milk",
        hint: "./images/milk.png"
    }, 
    {
        word: "bear",
        hint: "./images/bear.png"
    }, 
    {
        word: "candy",
        hint: "./images/candy.png"
    }
]

let kidsWordsRo = [
    {
        word: "maimuta",
        hint:  "./images/monkey.png"
    },
    {
        word: "inghetata",
        hint: "./images/icecream.png"
    }, 
    {
        word: "calut",
        hint: "./images/horse.png"
    }, 
    {
        word: "bicicleta",
        hint: "./images/bicicle.png"
    }, 
    {
        word: "curcubeu",
        hint: "./images/rainbow.png"
    }, 
    {
        word: "carte",
        hint: "./images/book.png"
    }, 
    {
        word: "banana",
        hint: "./images/banana.png"
    }, 
    {
        word: "lapte",
        hint: "./images/milk.png"
    }, 
    {
        word: "ursulet",
        hint: "./images/bear.png"
    }, 
    {
        word: "acadea",
        hint: "./images/candy.png"
    }
]