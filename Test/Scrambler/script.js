const wordText = document.querySelector(".word"),
  scrambleLetters = document.querySelector(".scramble_letters"),
  hintText = document.querySelector(".hint"),
  refreshBtn = document.querySelector(".refresh-word"),
  checkBtn = document.querySelector(".check-word"),
  tutorial = document.querySelector(".tutorial"),
  closeBtn = document.getElementById("closeBtn"),
  restartBtn = document.querySelector(".restart"),
  startBtn = document.getElementById("start-btn"),
  mainMenu = document.querySelector(".menu"),
  game = document.querySelector(".game"),
  goBackBtn = document.querySelector(".go-back-btn"),
  chestii = document.querySelector(".chestii"),
  practiceBtn = document.querySelector(".practice"),
  RPS = document.querySelector(".RPS"),
  mainPageText = document.querySelector(".menu-content p"),
  options = document.querySelectorAll(".options button"),
  playerHand = document.querySelector(".player-hand"),
  computerHand = document.querySelector(".computer-hand"),
  hands = document.querySelectorAll(".hands img"),
  rockBtn = document.querySelector(".rock"),
  paperBtn = document.querySelector(".paper"),
  scissorsBtn = document.querySelector(".scissors"),
  winner = document.querySelector(".winner"),
  playBtn = document.querySelector(".intro button"),
  introScreen = document.querySelector(".intro"),
  match = document.querySelector(".match"),
  roBtn = document.querySelector(".ro"),
  engBtn = document.querySelector(".eng"),
  gameWin = document.querySelector(".gameWin"),
  gameWinText = document.querySelector(".gameWin p"),
  mainMenuBtn = document.querySelector(".mainMenuBtn"),
  kidsModeBtn = document.querySelector(".kidsMode");

const img = document.createElement("img");

let difficultyText;
let wordsFoundText;
let triesText;
let correctWord, timer;
let wordsFound = 0;
let randomObj;
let difficulty = 1;
let subject = 0;
let language = "eng";
let tries = 5;
let wordArray;
let cloneArray;
let originalArray;
let letters;
let wordIndex;
let practiceMode = false;
let play_rps = false;
let playerChoice;
let kidsMode = false;

//change the language for all elements in the page to romanian
roBtn.addEventListener("click", () => {
  language = "ro";
  if (subject === 0) {
    mainPageText.innerHTML = "Apasă pentru a alege tema";
  }
  changeThemeText();
  document.querySelector(".title-htp h1").innerHTML = "Cum să te joci";
  document.querySelector(".title-htp h2").innerHTML =
    "Ghicește cuvântul amestecat în 5 încercări.";
  document.querySelector(".firstListItem").innerHTML =
    "Fiecare încercare trebuie să fie una <span class=right>validă</span>.";
  document.querySelector(".secondListItem").innerHTML =
    "Dacă dai un răspuns <span class=wrong>greșit</span> literele se vor scutura și vei pierde o încercare";
  document.querySelector(".bottom h4").innerHTML = "Rundă de acomodare";
  document.querySelector(".bottom p").innerHTML =
    "Dacă vrei să te acomodezi cu jocul, poți încerca o scurtă rundă de acomodare înainte de a începe";
  document.querySelector(".practice").innerHTML = "Încearcă";
  document.querySelector(".dificulty").innerHTML =
    "Dificultate: " +
    "<span class='difficultyText span-design'>" +
    "Ușor" +
    "</span>";
  document.querySelector(".tries-left").innerHTML =
    "Încercări rămase: " +
    "<span class='triesText span-design'>" +
    "0" +
    "</span>";
  document.querySelector(
    ".words-found"
  ).innerHTML = `Cuvinte găsite: <span class="wordsFound span-design">`;
  refreshBtn.innerHTML = "Amestecă cuvânt";
  checkBtn.innerHTML = "Verifică cuvânt";
  goBackBtn.innerHTML = "< Înapoi la meniul principal";
  playBtn.innerHTML = "Joacă";
  document.querySelector(".intro h2").innerHTML =
    "joacă Piatră Hârtie Foarfecă dacă vrei o nouă încercare, <br/> altfel jocul va reîncepe în 30 de secunde";
  rockBtn.innerHTML = "Piatră";
  paperBtn.innerHTML = "Hârtie";
  scissorsBtn.innerHTML = "Foarfecă";
});

//change the language off all elements in the page back to english
engBtn.addEventListener("click", () => {
  language = "eng";
  if (subject === 0) {
    mainPageText.innerHTML = "Click to choose theme";
  }
  changeThemeText();
  document.querySelector(".title-htp h1").innerHTML = "How to play";
  document.querySelector(".title-htp h2").innerHTML =
    "Guess the scrambled word in 5 tries.";
  document.querySelector(".firstListItem").innerHTML =
    "Each guess must be <span class=right>vaid</span>.";
  document.querySelector(".secondListItem").innerHTML =
    "If you guess <span class=wrong>wrong</span> the letters will shake and you will lose a try.";
  document.querySelector(".bottom h4").innerHTML = "Pratice round";
  document.querySelector(".bottom p").innerHTML =
    "If you want to get to know the game better, you can try a short practice match before starting";
  document.querySelector(".practice").innerHTML = "Practice";
  document.querySelector(".dificulty").innerHTML =
    "Dificulty: " +
    "<span class='difficultyText span-design'>" +
    "Easy" +
    "</span>";
  document.querySelector(".tries-left").innerHTML =
    "Tries left: " + "<span class='triesText span-design'>" + "0" + "</span>";
  document.querySelector(
    ".words-found"
  ).innerHTML = `Words found <span class="wordsFound span-design">`;
  refreshBtn.innerHTML = "Shuffle Word";
  checkBtn.innerHTML = "Check Word";
  goBackBtn.innerHTML = "< Main Menu";
  playBtn.innerHTML = "Let's play";
  document.querySelector(".intro h2").innerHTML =
    "play Rock Paper and Scissors if you want a new try, <br/> otherwise the game will restart in 30 seconds ";
  rockBtn.innerHTML = "Rock";
  paperBtn.innerHTML = "Paper";
  scissorsBtn.innerHTML = "Scissors";
});

//start the game in kids mode
kidsModeBtn.addEventListener("click", () => {
  kidsMode = true;
  chosen();
  startGameFunc();
  kidsModeBtn.classList.add("hide");
});

//go back to main menu
goBackBtn.addEventListener("click", () => {
  window.location.reload();
});

//add event listener for each card on the home page to select theme
let arrayImgCarousel = Array.from(document.querySelectorAll(".carusel img"));
arrayImgCarousel.forEach((imgCarousel) =>
  imgCarousel.addEventListener("click", clickImgCarousel)
);

//gets all cards
let cards_shadow = Array.from(document.querySelectorAll(".card"));

//starts practice game
practiceBtn.addEventListener("click", () => {
  practiceMode = true;
  chosen();
  originalArray = cloneArray.slice();
  closeTutorial();
});

//closes tutorial windows and starts the game
function closeTutorial() {
  tutorial.classList.add("FadeOutAnimation");
  goBackBtn.classList.remove("hide");
  setTimeout(() => {
    tutorial.classList.remove("FadeOutAnimation");
    tutorial.classList.add("hide");
    game.classList.remove("hide");
    initGame();
  }, 1000);

  setTimeout(() => {
    game.classList.add("FadeInAnimation");
    setTimeout(() => {
      game.classList.remove("FadeInAnimation");
    }, 1000);
  }, 1000);
}
closeBtn.addEventListener("click", closeTutorial);

//calls startGameFunc
startBtn.addEventListener("click", () => {
  startGameFunc();
});

//changes screen elements to hide the menu and display the tutorial
function startGameFunc() {
  difficultyText = document.querySelector(".difficultyText");
  triesText = document.querySelector(".triesText");
  wordsFoundText = document.querySelector(".wordsFound");
  goBackBtn.classList.add("hide");
  roBtn.classList.add("hidden");
  engBtn.classList.add("hidden");

  mainMenu.classList.add("FadeOutAnimation");
  kidsModeBtn.classList.add("hide");
  setTimeout(() => {
    chestii.classList.remove("hide");
    mainMenu.classList.remove("FadeOutAnimation");
    mainMenu.classList.add("hide");
    tutorial.classList.remove("hide");
    originalArray = cloneArray.slice();
  }, 1000);
}

//gets the id of the clicked card (used to set the game theme)
function clickImgCarousel() {
  setTimeout(() => {
    startBtn.classList.remove("hide");
    const idCheckedInput = document.querySelector("input:checked").id;
    const imgSelectedCard = document.querySelector(
      `label[for = ${idCheckedInput}] img`
    ).src;
    let subjectSelector = document.querySelector("input:checked").value;
    document.body.style = `background-image: url('${imgSelectedCard}')`;
    subject = parseInt(subjectSelector);
    changeThemeText();
    chosen();
  }, 300);
  cards_shadow.forEach(
    (card_shadow) => (card_shadow.style.boxShadow = "0px 0px 50px 1px #000")
  );
}

//changes the theme based on the selected card
function changeThemeText() {
  if (language === "eng") {
    if (subject == 1) mainPageText.innerText = "City theme";
    else if (subject == 2) mainPageText.innerText = "Food theme";
    else if (subject == 3) mainPageText.innerText = "Brands theme";
  } else {
    if (subject == 1) mainPageText.innerText = "Temă orașe";
    else if (subject == 2) mainPageText.innerText = "Temă mâncare";
    else if (subject == 3) mainPageText.innerText = "Temă companii";
  }
}

//gets the array that corresponds to the selected game condition
function chosen() {
  //cities
  //difficulty easy for cities
  if (difficulty === 1 && subject === 1 && !practiceMode && !kidsMode) {
    if (language === "eng") {
      cloneArray = citiesEasy.slice();
    } else {
      cloneArray = citiesEasyRo.slice();
    }

    //difficulty medium for cities
  } else if (difficulty === 2 && subject === 1 && !practiceMode && !kidsMode) {
    if (language === "eng") {
      cloneArray = citiesMedium.slice();
    } else {
      cloneArray = citiesMediumRo.slice();
    }

    //difficulty hard for cities
  } else if (
    (difficulty === 3) & (subject === 1) &&
    !practiceMode &&
    !kidsMode
  ) {
    if (language === "eng") {
      cloneArray = citiesHard.slice();
    } else {
      cloneArray = citiesHardRo.slice();
    }

    //logos
    //difficulty easy for logos
  } else if (
    (difficulty === 1) & (subject === 3) &&
    !practiceMode &&
    !kidsMode
  ) {
    if (language === "eng") {
      cloneArray = logosEasy.slice();
    } else {
      cloneArray = logosEasyRo.slice();
    }

    //difficulty medium for logos
  } else if (
    (difficulty === 2) & (subject === 3) &&
    !practiceMode &&
    !kidsMode
  ) {
    if (language === "eng") {
      cloneArray = logosMedium.slice();
    } else {
      cloneArray = logosMediumRo.slice();
    }

    //difficulty hard for logos
  } else if (
    (difficulty === 3) & (subject === 3) &&
    !practiceMode &&
    !kidsMode
  ) {
    if (language === "eng") {
      cloneArray = logosHard.slice();
    } else {
      cloneArray = logosHardRo.slice();
    }

    //food
    //difficulty easy for food
  } else if (
    (difficulty === 1) & (subject === 2) &&
    !practiceMode &&
    !kidsMode
  ) {
    if (language === "eng") {
      cloneArray = foodEasy.slice();
    } else {
      cloneArray = foodEasyRo.slice();
    }

    //difficulty medium for food
  } else if (
    (difficulty === 2) & (subject === 2) &&
    !practiceMode &&
    !kidsMode
  ) {
    if (language === "eng") {
      cloneArray = foodMedium.slice();
    } else {
      cloneArray = foodMediumRo.slice();
    }
    //difficulty hard for food
  } else if (
    (difficulty === 3) & (subject === 2) &&
    !practiceMode &&
    !kidsMode
  ) {
    if (language === "eng") {
      cloneArray = foodHard.slice();
    } else {
      cloneArray = foodHardRo.slice();
    }
  } else if (practiceMode) {
    if (language === "eng") {
      cloneArray = wordsPractice.slice();
    } else {
      cloneArray = wordsPracticeRo.slice();
    }
  } else if (kidsMode) {
    if (language === "eng") {
      cloneArray = kidsWords.slice();
    } else {
      cloneArray = kidsWordsRo.slice();
    }
  }
}

//initialise the game
function initGame(initWord) {
  wordText.innerHTML = "";
  if (initWord) {
    wordArray = initWord;
  } else {
    randomObj = cloneArray[Math.floor(Math.random() * cloneArray.length)];
    //split the word letter by letter and place it in an array
    wordArray = randomObj.word.split("");
  }
  //swap array elements (letters) to scramble the word
  for (let i = 0; i < wordArray.length; i++) {
    let j = Math.floor(Math.random() * i);
    //swaps one letter with a random one from the same array
    [wordArray[i], wordArray[j]] = [wordArray[j], wordArray[i]];
  }
  scrambleLetters.innerHTML = "";

  //creates a div for each letter
  wordArray.forEach((letter) => {
    scrambleLetters.innerHTML += "<div>" + letter.toUpperCase() + "</div>";
  });

  //used to show image instead of text in kids mode
  if (kidsMode && !practiceMode) {
    img.src = randomObj.hint;
    img.style = `
    width:60px;
    height:60px`;
    hintText.classList.remove("hidden");
    hintText.appendChild(img);
  } else hintText.innerText = "";
  triesText.innerHTML = tries;
  if (tries == 5) play_rps = false;
  correctWord = randomObj.word.toLowerCase();
  // console.log(correctWord);

  //adds event listener of click on each letter div created
  letters = Array.from(document.querySelectorAll(".scramble_letters div"));
  letters.forEach((letter, index) => {
    letter.addEventListener("click", letterClick);
    setTimeout(() => {
      letter.classList.add("show_letter");
    }, index * 150);
  });

  //sets difficulty text based on the language and level
  if (difficulty === 1) {
    if (language === "eng") {
      difficultyText.innerHTML = "Easy";
    } else {
      difficultyText.innerHTML = "Ușor";
    }
  } else if (difficulty === 2) {
    if (language === "eng") {
      difficultyText.innerHTML = "Medium";
    } else {
      difficultyText.innerHTML = "Mediu";
    }
  } else if (difficulty === 3) {
    if (language === "eng") {
      difficultyText.innerHTML = "Hard";
    } else {
      difficultyText.innerHTML = "Greu";
    }
  }
  //sets text and language depending of the game mode
  if (practiceMode) {
    wordsFoundText.innerHTML = wordsFound + "/4";
    if (language === "eng") {
      difficultyText.innerHTML = "Practice";
    } else {
      difficultyText.innerHTML = "Încercare";
    }
  } else {
    wordsFoundText.innerHTML = wordsFound + "/10";
  }
  if (kidsMode) {
    if (language === "eng") {
      difficultyText.innerHTML = "Kids mode";
    } else {
      difficultyText.innerHTML = "Mod copii";
    }
  }
}

//check if the entered word is correct
const checkWord = () => {
  let userWord = wordText.innerText.toLowerCase();
  let word = document.querySelectorAll(".word span");
  const difficultyInfo = document.querySelector(".difficultyInfo");
  const difficultyInfoText = document.querySelector(".difficultyInfo p");
  //if the word is wrong decrement tries
  if (userWord !== correctWord) {
    tries--;
    //display the hint correctly depending on the game mode
    if (tries <= 2 && !kidsMode) {
      hintText.classList.remove("hidden");
      hintText.innerText = "Hint: " + randomObj.hint;
    }
    if (tries <= 2 && kidsMode && practiceMode) {
      hintText.classList.remove("hidden");
      hintText.innerText = "Hint: " + randomObj.hint;
    }
    // if RPS was not played and there are no tries, then you can play RPS
    if (tries === 0) {
      if (!play_rps) {
        openRPSGame();
        RPS.classList.remove("hide");
        game.classList.add("hide");
        return;
      } else {
        if (language == "eng") {
          difficultyInfoText.innerHTML =
            "You lost! <br/>The current game level will restart";
        } else {
          difficultyInfoText.innerHTML =
            "Ai pierdut! <br/>Nivelul curent de joc va reincepe";
        }
        difficultyInfo.classList.remove("hide");
        setTimeout(() => {
          difficultyInfo.classList.add("hide");
          chosen();
          wordsFound = 0;
          tries = 5;
          initGame();
        }, 3000);
      }
    }

    //shakes the placed letters if the word is wrong
    word.forEach((letter) => {
      letter.classList.add("shake_animation");
      setTimeout(() => {
        letters.forEach((scramble_letter) => {
          scramble_letter.style.opacity = "1";
          scramble_letter.style.pointerEvents = "all";
          scramble_letter.style.backgroundColor = "transparent";
        });
        wordText.innerHTML = "";
      }, 500);
    });
    triesText.innerHTML = tries;
    return;
  }

  //animation for when the word is correct
  word.forEach((letter, index) => {
    letter.style.animation = "happy_letters 1s alternate 2";
    letter.style.animationDelay = `${index * 100}ms`;
  });
  //increments found words
  wordsFound++;
  if (practiceMode) {
    wordsFoundText.innerHTML = wordsFound + "/4";
  } else {
    wordsFoundText.innerHTML = wordsFound + "/10";
  }

  tries = 5;

  //remove found element from array
  wordIndex = cloneArray.findIndex((object) => {
    return object.word === correctWord;
  });
  cloneArray.splice(wordIndex, 1);
  setTimeout(() => {
    //removes the image when the word changes in kids mode
    if (kidsMode && !practiceMode) {
      hintText.removeChild(img);
    }
    //changes fro practice mode to the game
    if (practiceMode && wordsFound === 4) {
      if (language === "eng") {
        difficultyInfoText.innerHTML =
          "Practice mode is over, now to the real game";
      } else {
        difficultyInfoText.innerHTML =
          "Modul de încercare s-a terminat, acum hai să ne jucăm cu adevărat.";
      }
      difficultyInfo.classList.remove("hide");
      setTimeout(() => {
        difficultyInfo.classList.add("hide");
      }, 3000);
      wordsFound = 0;
      practiceMode = false;
      chosen();
      originalArray = cloneArray.slice();
      initGame();
    }
    //condition to chenge the difficulty
    if (wordsFound === 10 && !practiceMode && difficulty != 3 && !kidsMode) {
      if (language === "eng") {
        difficultyInfoText.innerHTML =
          "<p>Congratulations you beat this level.<br />Difficulty has been increased</p>";
      } else {
        difficultyInfoText.innerHTML =
          "<p>Felicitări ai terminat acest nivel.<br />Dificultatea a fost mărită</p>";
      }
      difficultyInfo.classList.remove("hide");
      setTimeout(() => {
        difficultyInfo.classList.add("hide");
      }, 3000);
      difficulty++;
      wordsFound = 0;
      chosen();
      originalArray = cloneArray.slice();
      //winning condition
    } else if (wordsFound === 10 && (difficulty === 3 || kidsMode)) {
      if (language === "eng") {
        gameWin.classList.remove("hide");
        gameWin.style = "flex-direction:column";
        gameWinText.innerHTML = "Congratulations you won";
      } else {
        gameWin.classList.remove("hide");
        gameWinText.innerHTML = "Felicitări ai câștigat";
        gameWin.style = "flex-direction:column";
        mainMenuBtn.innerHTML = "Înapoi la meniul principal";
      }
    }
    initGame();
  }, 2200);
};

//button to return to main menu
mainMenuBtn.addEventListener("click", () => {
  window.location.reload();
});

//restart the game
function restart() {
  //reinitialise array to default values after restart
  cloneArray = originalArray.slice();

  //reinitialise array to default values after restart
  cloneArray = originalArray.slice();
  wordsFound = 0;
  tries = 5;
  initGame();
}

//suffles the word again
function refreshScrambleLetters() {
  initGame(wordArray);
}

//changes the aspect of the clicked letters and prevents another click
function letterClick(e) {
  let clickedDiv = e.target;
  clickedDiv.style.opacity = "0.3";
  clickedDiv.style.pointerEvents = "none";
  clickedDiv.style.backgroundColor = "gray";
  let selectedLetter = clickedDiv.innerHTML;
  wordText.innerHTML += "<span>" + selectedLetter + "</span>";
}

//event listeners for buttons
refreshBtn.addEventListener("click", refreshScrambleLetters);
checkBtn.addEventListener("click", checkWord);
restartBtn.addEventListener("click", restart);

paperBtn.addEventListener("click", () => {
  playerChoice = "paper";
});
rockBtn.addEventListener("click", () => {
  playerChoice = "rock";
});
scissorsBtn.addEventListener("click", () => {
  playerChoice = "scissors";
});

// let's play button RPS
playBtn.addEventListener("click", () => {
  introScreen.classList.add("fadeOut");
  match.classList.add("fadeIn");
  clearTimeout(timer);
});

// remove animation from style when hands animation end
hands.forEach((hand) => {
  hand.addEventListener("animationend", function () {
    this.style.animation = "";
  });
});

options.forEach((option) => {
  option.addEventListener("click", playRPS);
});

// open RPS
function openRPSGame() {
  // start game after 30 seconds if not click on play button
  timer = setTimeout(() => {
    RPS.classList.add("hide");
    game.classList.remove("hide");
    chosen();
    wordsFound = 0;
    tries = 5;
    initGame();
  }, 30000);
  // show options to choose for RPS
  introScreen.classList.remove("fadeOut");
  match.classList.remove("fadeIn");
  if (language == "eng") {
    winner.textContent = "Choose an option";
  } else {
    winner.textContent = "Alege o opțiune";
  }
  playerHand.src = "./images/rock.png";
  computerHand.src = "./images/rock.png";
};

//Play RPS
function playRPS() {
  //Computer Options
  const computerOptions = ["rock", "paper", "scissors"];
  //Computer Choice
  const computerNumber = Math.floor(Math.random() * 3);
  const computerChoice = computerOptions[computerNumber];

  setTimeout(() => {
    console.log('compare hands');
    //Here is where we call compare hands
    compareHands(playerChoice, computerChoice);
    //Update Images
    playerHand.src = `./images/${playerChoice}.png`;
    computerHand.src = `./images/${computerChoice}.png`;
  }, 2000);
  //Animation
  playerHand.style.animation = "shakePlayer 2s ease";
  computerHand.style.animation = "shakeComputer 2s ease";
};


const compareHands = (playerChoice, computerChoice) => {
  const difficultyInfo = document.querySelector(".difficultyInfo");
  const difficultyInfoText = document.querySelector(".difficultyInfo p");

  //Checking for a tie
  if (playerChoice === computerChoice) {
    if (language == "eng") {
      winner.textContent = "It is a tie";
    } else {
      winner.textContent = "Egalitate";
    }
    return;
  } else if (
    (playerChoice === "rock" && computerChoice === "scissors") ||
    (playerChoice === "paper" && computerChoice === "rock") ||
    (playerChoice === "scissors" && computerChoice === "paper")
  ) {
    // show RPS game result
    if (language == "eng") {
      winner.textContent = "You Won";
    } else {
      winner.textContent = "Ai câștigat";
    }
    // player won then add one more try
    tries++;
    play_rps = true;
    setTimeout(() => {
      RPS.classList.add("hide");
      game.classList.remove("hide");
    }, 2000);
    return;
  } else if (
    (computerChoice === "rock" && playerChoice === "scissors") ||
    (computerChoice === "paper" && playerChoice === "rock") ||
    (computerChoice === "scissors" && playerChoice === "paper")
  ) {
    // show RPS game result
    if (language == "eng") {
      winner.textContent = "Computer Won";
    } else {
      winner.textContent = "Calculatorul a câștigat";
    }
    // show scramble game result, then restart game
    setTimeout(() => {
      if (language == "eng") {
        difficultyInfoText.innerHTML =
          "You lost! <br/>The current game level will restart in 30 seconds.";
      } else {
        difficultyInfoText.innerHTML =
          "Ai pierdut! <br/>Nivelul curent de joc va reincepe in 30 de secunde.";
      }
      difficultyInfo.classList.remove("hide");
      tries = 5;
      play_rps = true;
      setTimeout(() => {
        difficultyInfo.classList.add("hide");
        RPS.classList.add("hide");
        game.classList.remove("hide");
        chosen();
        wordsFound = 0;
        initGame();
      }, 30000);
    }, 2000);

    return;
  }
};
